<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Payment\Block\Payment\Method;

use Exception;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Phrase;
use Magento\Framework\Pricing\Helper\Data;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;
use Magento\Payment\Block\Info;
use Magento\Sales\Model\OrderRepository;
use Magento\Store\Model\ScopeInterface;
use Magestore\Webpos\Api\Data\Payment\PaymentInterface;
use Magestore\Webpos\Helper\Payment;
use Magestore\Webpos\Model\ResourceModel\Sales\Order\Payment\Collection;
use Magestore\Webpos\Model\ResourceModel\Sales\Order\Payment\CollectionFactory;
use Psr\Log\LoggerInterface;

/**
 * Payment method MultiPayment
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class MultiPayment extends Info
{
    /**
     * Helper payment object
     *
     * @var Payment
     */
    protected $_helperPricing = '';

    /**
     * @var CollectionFactory|string
     */
    protected $orderPaymentCollectionFactory = '';

    /**
     * Model order repository
     *
     * @var OrderRepository
     */
    protected $_orderRepository = '';

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry = null;

    /**
     * MultiPayment constructor.
     *
     * @param Context $context
     * @param Data $helperPricing
     * @param CollectionFactory $orderPaymentCollectionFactory
     * @param OrderRepository $orderRepository
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $helperPricing,
        CollectionFactory $orderPaymentCollectionFactory,
        OrderRepository $orderRepository,
        Registry $registry,
        array $data = []
    ) {

        $this->_helperPricing = $helperPricing;
        $this->orderPaymentCollectionFactory = $orderPaymentCollectionFactory;
        $this->_orderRepository = $orderRepository;
        $this->_coreRegistry = $registry;
        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * Construct function
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('Magestore_Payment::payment/method/info/multi_payment.phtml');
    }

    /**
     * Get method title from setting
     *
     * @return Phrase|mixed
     */
    public function getMethodTitle()
    {
        $title = $this->_scopeConfig->getValue(
            'payment/multipaymentforpos/title',
            ScopeInterface::SCOPE_STORE
        );
        if ($title == '') {
            $title = __('Split Payments');
        }
        return $title;
    }

    /**
     * Get Specific Information
     *
     * @return array
     * @throws LocalizedException
     */
    public function getSpecificInformation()
    {
        $specificInformation = [];
        $actualTotalPaid = 0;
        foreach ($this->getOrderPaymentMethods() as $paymentMethod) {
            if ($paymentMethod->getData('base_amount_paid') > 0) {
                $actualTotalPaid += $paymentMethod->getData('base_amount_paid');
                $specificInformation[] = [
                    'label' => $paymentMethod->getData('title'),
                    'value' => $this->_helperPricing->currency(
                        $paymentMethod->getData('base_amount_paid'),
                        true,
                        false
                    ),
                    'reference_number' => $paymentMethod->getData('reference_number'),
                    'card_type' => $paymentMethod->getData('card_type')
                ];
            }
        }
        $orderId = $this->getInfo()->getData('parent_id');
        $baseTotalPaid = 0;
        if ($this->_coreRegistry->registry('current_order')) {
            $baseTotalPaid = $this->_coreRegistry->registry('current_order')->getBaseTotalPaid();
        } else {
            try {
                $baseTotalPaid = $this->_orderRepository->get($orderId)->getBaseTotalPaid();
            } catch (Exception $e) {
                ObjectManager::getInstance()
                    ->get(LoggerInterface::class)
                    ->info($e->getMessage());
            }
        }
        if ($baseTotalPaid !== 0) {
            if ($actualTotalPaid < $baseTotalPaid) {
                array_push($specificInformation, [
                    'label' => __('Other'),
                    'value' => $this->_helperPricing->currency($baseTotalPaid - $actualTotalPaid, true, false),
                ]);
            }
        }
        return $specificInformation;
    }

    /**
     * Get Order Payment Methods
     *
     * @return Collection
     * @throws LocalizedException
     */
    public function getOrderPaymentMethods()
    {
        $orderId = $this->getInfo()->getData('parent_id');
        $payments = $this->orderPaymentCollectionFactory->create()
            ->addFieldToFilter('order_id', $orderId)
            ->addFieldToFilter('type', PaymentInterface::ORDER_TYPE);
        return $payments;
    }
}
