<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Payment\Block\Payment\Method;

use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Phrase;
use Magento\Framework\Pricing\Helper\Data;
use Magento\Framework\View\Element\Template\Context;
use Magento\Payment\Block\Info;
use Magento\Store\Model\ScopeInterface;
use Magestore\Webpos\Model\ResourceModel\Sales\Order\Payment\Collection;

/**
 * Payment method ReferencePaymentAbstract
 */
class ReferencePaymentAbstract extends Info
{
    /**
     * @var Collection
     */
    protected $orderPaymentCollection;

    /**
     * @var Data
     */
    protected $helperPricing;

    /**
     * ReferencePaymentAbstract constructor.
     *
     * @param Context $context
     * @param Data $helperPricing
     * @param Collection $orderPaymentCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $helperPricing,
        Collection $orderPaymentCollection,
        array $data = []
    ) {
        $this->orderPaymentCollection = $orderPaymentCollection;
        $this->helperPricing = $helperPricing;

        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * Prepare Specific Information
     *
     * @param string|null $transport
     * @return array|DataObject|null
     * @throws LocalizedException
     */
    protected function _prepareSpecificInformation($transport = null)
    {
        if (null !== $this->_paymentSpecificInformation) {
            return $this->_paymentSpecificInformation;
        }
        $data = [];
        $orderId = $this->getInfo()->getData('parent_id');
        $code = $this->getInfo()->getData('method');
        $amount = $this->getPaymentAmount($orderId, $code);
        if ($amount) {
            $referenceLabel = __('Reference No');
            $data[(string)$referenceLabel] = $this->helperPricing->currency($amount, true, false);
        }
        $transport = parent::_prepareSpecificInformation($transport);
        return $transport->setData(array_merge($data, $transport->getData()));
    }

    /**
     * Get Payment Amount
     *
     * @param int $orderId
     * @param string $code
     * @return int
     */
    public function getPaymentAmount($orderId, $code)
    {
        $payments = $this->orderPaymentCollection
            ->addFieldToFilter('order_id', $orderId)
            ->addFieldToFilter('method', $code);
        $amount = 0;
        if ($payments->getSize() > 0) {
            $payment = $payments->getFirstItem();
            $amount = $payment->getRealAmount();
        }
        return $amount;
    }

    /**
     * Get method title from setting
     *
     * @return Phrase|mixed
     */
    public function getMethodTitle()
    {
        $title = $this->_scopeConfig->getValue(
            'payment/cashforpos/title',
            ScopeInterface::SCOPE_STORE
        );
        if ($title == '') {
            $title = __("Cash");
        }
        return $title;
    }

    /**
     * Get Credit Card Method Title
     *
     * @return string
     */
    public function getCreditCardMethodTitle()
    {
        $title = $this->_scopeConfig->getValue('payment/ccforpos/title', ScopeInterface::SCOPE_STORE);
        if ($title == '') {
            $title = __("Credit Card");
        }
        return $title;
    }
}
