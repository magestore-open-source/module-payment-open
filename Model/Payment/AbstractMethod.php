<?php

/**
 * This file is part of Magestore POS Open Source.
 *
 * Magestore POS Open Source is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * Magestore POS Open Source is distributed in the hope that it will
 * be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Magestore POS Open Source. If not, see <https://www.gnu.org/licenses/>
 */

namespace Magestore\Payment\Model\Payment;

use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Payment\Helper\Data;
use Magento\Payment\Model\Method\Logger;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Abstract payment method
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class AbstractMethod extends \Magento\Payment\Model\Method\AbstractMethod
{
    const ALLOW_SPECIFIC_PAYMENT = 'webpos/payment/allowspecific_payment';
    const SPECIFIC_PAYMENT = 'webpos/payment/specificpayment';

    /**
     * @var string
     */
    protected $enabledPath = '';

    /**
     * @var Http
     */
    protected $request;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param Data $paymentData
     * @param ScopeConfigInterface $scopeConfig
     * @param Logger $logger
     * @param Http $request
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        Data $paymentData,
        ScopeConfigInterface $scopeConfig,
        Logger $logger,
        Http $request,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->request = $request;

        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Enable for Web POS only
     *
     * @param CartInterface|null $quote
     * @return bool
     * @throws LocalizedException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function isAvailable(CartInterface $quote = null)
    {
        $routeName = $this->request->getRouteName();
        $settingEnabled = $this->isEnabledSetting();
        if ($routeName == "webpos" && $settingEnabled == true) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Is Enabled Setting
     *
     * @return bool
     * @throws LocalizedException
     */
    public function isEnabledSetting()
    {
        return (
            $this->_scopeConfig->getValue($this->enabledPath, ScopeInterface::SCOPE_STORE)
            && $this->isAllowOnWebPOS($this->getCode())
        );
    }

    /**
     * Is Allow On WebPOS
     *
     * @param string $code
     * @return boolean
     */
    public function isAllowOnWebPOS($code)
    {
        if ($this->_scopeConfig->getValue(self::ALLOW_SPECIFIC_PAYMENT, ScopeInterface::SCOPE_STORE) == '1') {
            $specificpayment = $this->_scopeConfig->getValue(self::SPECIFIC_PAYMENT, ScopeInterface::SCOPE_STORE);
            $specificpayment = explode(',', $specificpayment);
            if (in_array($code, $specificpayment)) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }
}
